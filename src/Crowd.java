import java.util.ArrayList;
import java.util.List;


public class Crowd {

	List<Group> groupList ;

	public Crowd() {
		groupList = new ArrayList<Group>();
	}

	public List<Group> getGroupList() {
		return groupList;
	}
	
	public void addGroup(Group g) {
		groupList.add(g);
	}
}
