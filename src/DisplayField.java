import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

import javax.vecmath.Vector4d;

import org.apache.xmlbeans.impl.xb.xsdschema.Public;

import repast.simphony.context.Context;
import repast.simphony.space.continuous.ContinuousSpace;
import repast.simphony.visualizationOGL2D.DefaultStyleOGL2D;
import sun.management.resources.agent;





public class DisplayField {

	FieldAgent[][] agents;
	int width;
	int height;
	
	
	public DisplayField(Fields fields, Context<Agent> context, ContinuousSpace<Agent> space) {
		Double[][] density = fields.density;
		
		width = fields.width;
		height = fields.height;
		agents = new FieldAgent[width][height];
		//Vector4d[][][] pot = new Vector4d[width][height][fields.crowd.groupList.size()];

		for (int i = 0; i < width; ++i){
			for (int j = 0; j < height; ++j) {
				Double d = density[i][j];
				FieldAgent a = new FieldAgent(d, i, j);
				context.add(a);
				space.moveTo(a, i, j);
				agents[i][j]= a;
			}

		}
	}
	
	public void updateAgentsValue(Double[][] density)
	{
		for (int i = 0; i < width; ++i){
			for (int j = 0; j < height; ++j) {
				Double d = density[i][j];
				agents[i][j].value = d; 
			}
		}
	}
}
