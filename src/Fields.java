import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.List;

import javax.vecmath.Point2d;
import javax.vecmath.Vector2d;
import javax.vecmath.Vector2f;
import javax.vecmath.Vector4d;

import org.hsqldb.rights.User;

import com.jogamp.opengl.math.geom.AABBox;

import repast.simphony.engine.schedule.ScheduledMethod;


public class Fields extends Agent {
	Double[][] density;
	Vector2d[][] average_velocity;
	int width;
	int height;
	Crowd crowd;

	Vector4d[][] speed;
	Vector4d[][] cost;

	double max_speed = 0.7;
	double min_density = 0.2;
	double max_density = 5;
	double min_flow_speed = 0.02;
	double lambda = -Math.log(min_density / 2.);

	private double min_distance = 0.2; // minimal distance btw user == radius user

	DisplayField df;

	public Fields(Crowd crowd, int w, int h) {

		density = new Double[w][h];
		average_velocity = new Vector2d[w][h];

		speed = new Vector4d[w][h];
		cost = new Vector4d[w][h];


		this.width = w;
		this.height = h;
		this.crowd = crowd;

		for (int j = 0; j < h; ++j)
			for (int i = 0; i < w; ++i) {
				density[i][j] = min_density;


				speed[i][j] = new Vector4d();
				speed[i][j].set(min_flow_speed, min_flow_speed, min_flow_speed, min_flow_speed);

				cost[i][j] = new Vector4d();
				cost[i][j].set(0, 0, 0, 0);
				average_velocity[i][j] = new Vector2d();
			}
	}

	public void resetDensityAndAverageVelocity()
	{
		for (int i = 0; i < width; i++) {
			for (int j = 0; j < height; ++j) {
				density[i][j] = 0d;
				average_velocity[i][j].set(0, 0);
			}
		}
	}
	//check if they are more than or less than the max and min density, if yes, reset them. at the same time, calculate the average velocity
	public void normalizeDensityAndAverageVelocity()
	{
		for (int i = 0; i < width; i++) {
			for (int j = 0; j < height; ++j) {

				if(density[i][j]<=0) 
				{
					//when the density is 0, flow speed is also zero, i.e. ave_vel=0£¬
					density[i][j]=min_density;
					
				}
				else 
				{
					density[i][j] = Math.min(max_density, density[i][j]);
					density[i][j] = Math.max(min_density, density[i][j]);
					average_velocity[i][j].x /= density[i][j];
					average_velocity[i][j].y /= density[i][j];
					//System.out.println(average_velocity[i][j]);
				}
			}
		}
	}

	public void computeDensityAndVelocityFields()
	{
		resetDensityAndAverageVelocity();

		List<Group> groups = crowd.getGroupList();
		for (int g = 0; g < groups.size(); ++g){
			List<UserAgent> agentList = (groups.get(g)).getAgentList();
			for (int p = 0; p < agentList.size(); ++p) {
				UserAgent a = agentList.get(p);

				int cx = (int) Math.floor(a.x); // Cell of the agent 
				int cy = (int) Math.floor(a.y);
				double dx = a.x - cx; 
				double dy = a.y - cy;


				// out of map
				if (cx < 0 || cy < 0 || cx >= width || cy >= height)
					continue;


				// Compute density Fields
				
				// A Bottom Left
					setDensityAndAverageVelocity(Math.pow(Math.min(1-dx, 1-dy), lambda), cx, cy, a);
				if (cx+1 < width) // B Bottom Right10
					setDensityAndAverageVelocity(Math.pow(Math.min(dx, 1-dy), lambda), cx+1, cy, a);
				if (cy+1  < height) // D Top Left
					setDensityAndAverageVelocity(Math.pow(Math.min(1-dx, dy), lambda), cx, cy+1, a);
				if (cx+1 < width && cy+1 < height) // C Top Right
					setDensityAndAverageVelocity(Math.pow(Math.min(dx, dy), lambda), cx+1, cy+1, a);

			}
		}
		normalizeDensityAndAverageVelocity();
	}
	public void setDensityAndAverageVelocity(double d, int x, int y, UserAgent a)
	{
		density[x][y] += d;
		Vector2d av = new Vector2d(d * a.speed * a.dir_x,
								   d * a.speed * a.dir_y);
		average_velocity[x][y].add(av);
	}
	
	public void computeCost()
	{
		double path_weight = 1;
		double time_weight = 3;

		for (int j = 0; j < height; ++j)
			for (int i = 0; i < width; ++i) {
				Vector4d spd = speed[i][j];
				double discomfort_weight = density[i][j];
				
				double cN = (path_weight * spd.w + time_weight + discomfort_weight) / spd.w;
				double cS = (path_weight * spd.x + time_weight + discomfort_weight) / spd.x;
				double cE = (path_weight * spd.y + time_weight + discomfort_weight) / spd.y;
				double cW = (path_weight * spd.z + time_weight + discomfort_weight) / spd.z;
				cost[i][j].set(cN, cS, cE, cW);
			}
	}

	public void updateSpeedField()
	{
		for (int i = 0; i < width; ++i){
			for (int j = 0; j < height; ++j) {
				double coef = max_speed + ((density[i][j] - min_density) / (max_density-min_density));


				int xe = (i+1 < width) ? i+1 : i;
				double sE = Math.abs(coef * (average_velocity[xe][j].x - max_speed));
				sE = Math.max(sE, min_flow_speed);

				int xw = (i-1 >= 0) ? i-1 : i;
				double sW = Math.abs(coef * (average_velocity[xw][j].x - max_speed));
				sW = Math.max(sW, min_flow_speed);

				int yn = (j+1 < height) ? j+1: j;
				double sN = Math.abs(coef * (average_velocity[i][yn].y - max_speed));
				sN = Math.max(sN, min_flow_speed);

				int ys = (j-1 >= 0) ? j-1 : j;
				double sS = Math.abs(coef * (average_velocity[i][ys].y - max_speed));
				sS = Math.max(sS, min_flow_speed);

				speed[i][j].set(sN, sS, sE, sW);
				//System.out.println(speed[i][j]);
			}
		}

	}
	
	public void MinimumDistanceEnforcement()
	{
		for (int i1 = 0; i1 < crowd.groupList.size(); ++i1){
			Group g1 = crowd.groupList.get(i1);
			for (UserAgent a1 : g1.agentList) {				
				for (int i2 = 0; i2 < crowd.groupList.size(); ++i2) {
					Group g2 = crowd.groupList.get(i2);
					for (UserAgent a2 : g2.agentList) {
						
						double d = Math.sqrt(Math.pow(a1.x-a2.x, 2) + Math.pow(a1.y-a2.y, 2));
						if (d != 0 && d < min_distance) {
							a1.x += (min_distance * (a2.dir_x+a1.dir_x))/2;
							a1.y += (min_distance * (a2.dir_y+a1.dir_y))/2;
							a2.x += (min_distance * (a1.dir_x+a2.dir_x))/2;
							a2.y += (min_distance * (a1.dir_y+a2.dir_y))/2;
						}
							
						

					}
				}
			}
		}
	}


	@ScheduledMethod(start = 1, interval = 1, priority = 1)
	public void update()
	{
		computeDensityAndVelocityFields();
		updateSpeedField();
		computeCost();
		
		for (Group g : crowd.groupList)
		{
			g.computePotentionalAndGradientMap();
			g.updateVelocityAndPosition();
		}
		
		
		MinimumDistanceEnforcement();

		
		
		// FIXME
		df.updateAgentsValue(density);
	}

}
