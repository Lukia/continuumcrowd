public class FieldAgent extends Agent
{
	double value;
	int x;
	int y;

	public FieldAgent(double value, int x, int y) {
		this.value = value;
		this.x = x;
		this.y = y;
	}
}