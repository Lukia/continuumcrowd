import java.awt.Color;

import repast.simphony.visualizationOGL2D.DefaultStyleOGL2D;
import saf.v3d.scene.VSpatial;

public class FieldStyle extends DefaultStyleOGL2D
{
	@Override
	public Color getColor(Object agent) {
		if (agent instanceof FieldAgent)
		{
			float v = Math.min((float) ((FieldAgent) agent).value, 1f)  ;
			Color c = new Color(v, v, v);
			return c;
		}
		return Color.BLACK;
	}
	
	@Override
	public VSpatial getVSpatial(Object agent, VSpatial spatial) {
		if (spatial == null) {
			spatial = shapeFactory.createRectangle(15, 15);
		}
		return spatial;
	}
}
