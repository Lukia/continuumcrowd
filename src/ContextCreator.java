

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.Iterator;

import repast.simphony.context.Context;
import repast.simphony.context.space.continuous.ContinuousSpaceFactory;
import repast.simphony.context.space.continuous.ContinuousSpaceFactoryFinder;
import repast.simphony.context.space.continuous.DefaultContinuousSpaceFactory;
import repast.simphony.context.space.grid.GridFactory;
import repast.simphony.context.space.grid.GridFactoryFinder;
import repast.simphony.dataLoader.ContextBuilder;
import repast.simphony.engine.environment.RunEnvironment;
import repast.simphony.space.continuous.ContinuousSpace;
import repast.simphony.space.continuous.RandomCartesianAdder;
import repast.simphony.space.continuous.SimpleCartesianAdder;
import repast.simphony.space.continuous.StrictBorders;
import repast.simphony.space.grid.Grid;
import repast.simphony.space.grid.GridBuilderParameters;
import repast.simphony.space.grid.SimpleGridAdder;

public class ContextCreator implements ContextBuilder<Agent>
{

	@Override
	public Context<Agent> build(Context<Agent> context)
	{
		context.setId("ContinuumCrowd");


		int w = 50;
		int h = 50;

		ContinuousSpaceFactory spFact = ContinuousSpaceFactoryFinder.createContinuousSpaceFactory(null);
		ContinuousSpace<Agent> userSpace = spFact.createContinuousSpace("userSpace", context, new SimpleCartesianAdder<Agent>(), new StrictBorders(), w, h);


		
		Crowd crowd = new Crowd();
		Fields fields = new Fields(crowd, w, h);
		DisplayField df = new DisplayField(fields, context, userSpace);

		fields.df = df;
		
		
		for (int g = 0; g < 2; ++g) {
			//Group group = new Group((int)(Math.random()*w), (int)(Math.random()*h), g, w, h, fields.cost, fields.speed, userSpace);
			Group group = new Group(48*g, 48*g, g, w, h, fields.cost, fields.speed, userSpace);

			for (int i = 0; i < 500; ++i){
				double x = Math.random() * w;
				double y = Math.random() * h;
				UserAgent a = new UserAgent(x, y, g);
				context.add(a);
				userSpace.moveTo(a, x, y);
				group.addAgent(a);
			}
			crowd.addGroup(group);
		}
		context.add(fields);

		/*
			GridFactory gFact = GridFactoryFinder.createGridFactory(null);

			GridBuilderParameters<Agent> gridP = new GridBuilderParameters<Agent>(
					new repast.simphony.space.grid.StrictBorders(), 
					new SimpleGridAdder<Agent>(), true, 50,50);
			Grid<Agent> grid = gFact.createGrid("grid", context, gridP);
		 */

		
		

		return context;
	}
}