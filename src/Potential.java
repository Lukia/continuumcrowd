enum PotentialState 
{
	KNOWN,
	UNKNOWN,
	CANDIDATE
};


public class Potential {
	

	double potential;
	double grad_x=0;
	double grad_y=0;
	int x;
	int y;
	
	PotentialState state;
	
	public void set(double potential, int x, int y, PotentialState state) {
		this.potential = potential;
		this.x = x;
		this.y = y;
		this.state = state;
	}
}
