import java.awt.Color;

import repast.simphony.visualization.visualization3D.ShapeFactory;
import repast.simphony.visualizationOGL2D.DefaultStyleOGL2D;
import saf.v3d.scene.VSpatial;


public class UserAgentStyle extends DefaultStyleOGL2D {

	private Color[] colors = new Color[] { Color.red, Color.green, Color.pink, Color.yellow, Color.cyan, Color.magenta, Color.blue };
	@Override
	public Color getColor(Object agent) {
		if (agent instanceof UserAgent){
			Color c = colors[((UserAgent)agent).groupId % colors.length];
			return c;
		}
		return Color.black;
	}


	@Override
	public VSpatial getVSpatial(Object agent, VSpatial spatial) {
		if (spatial == null) {
			spatial = shapeFactory.createCircle(3, 10);
		}
		return spatial;
	}
}