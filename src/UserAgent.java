

import repast.simphony.engine.schedule.ScheduledMethod;

public class UserAgent extends Agent{

	protected double x;
	protected double y;
	protected boolean display;
	public double speed;
	public double dir_x;
	public double dir_y;
	
	public int groupId;
	
	public UserAgent(double x, double y, int groupId){
		this.x = x;
		this.y = y;
		this.display = true;
		this.groupId = groupId;
	}
	
	
	public double getX(){
		return this.x;
	}
	
	public double getY(){
		return this.y;
	}
	
	public void moveTo(double  x, double y)
	{
		this.x = x;
		this.y = y;
	}

	public boolean isDisplay()
	{
		return display;
	}

	public void setDisplay(boolean display)
	{
		this.display = display;
	}
}
