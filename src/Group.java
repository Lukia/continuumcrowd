import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.PriorityQueue;

import javax.vecmath.Vector2d;
import javax.vecmath.Vector4d;

import org.apache.commons.math3.analysis.function.Max;
import org.apache.commons.math3.analysis.function.Min;
import org.stringtemplate.v4.compiler.CodeGenerator.conditional_return;

import bsh.commands.dir;
import repast.simphony.context.Context;
import repast.simphony.space.continuous.ContinuousSpace;



public class Group {
	// FIXME 
	double timestep = 0.02;

	int id;

	Potential[][] potential;
	List<UserAgent> agentList = new ArrayList<UserAgent>();

	int goal_x;
	int goal_y;

	private Vector4d[][] costMap;
	private Vector4d[][] speedMap;

	private int w;
	private int h;

	private int min_distance = 9; // minimal distance btw user == radius user


	private ContinuousSpace<Agent> space;
	private double HIGH = 99999d;

	public Group(int goal_x, int goal_y, int id, int w, int h, Vector4d[][] costMap, Vector4d[][] speedMap, ContinuousSpace<Agent> space) {

		this.goal_x = goal_x;
		this.goal_y = goal_y;
		potential = new Potential[w][h];
		for (int i = 0; i < w; ++i)
			for (int j= 0; j < h; ++j){
				potential[i][j] = new Potential();
				potential[i][j].set(HIGH, i, j, PotentialState.UNKNOWN);
				potential[i][j].grad_x = 0;
				potential[i][j].grad_y = 0;
			}
		this.costMap = costMap;
		this.speedMap = speedMap;
		this.h = h;
		this.w = w;
		this.space = space;

		this.id = id;
	}

	public List<UserAgent> getAgentList() {
		return agentList;
	}

	public void addAgent(UserAgent a) 
	{
		agentList.add(a);
	}


	public void computePotentionalandGradientCell(int nx, int ny)
	{
		// COMPUTE POTENTIONAL AND MIN DIRECTION
		potential[nx][ny].state = PotentialState.CANDIDATE;
		// w = NORTH; x = SOUTH; y = EAST; z = WEAST
		Direction mx, my;
		Vector4d cost = costMap[nx][ny];
		Vector4d pot = new Vector4d();


		// EAST
		pot.y = (nx+1 < h) ? potential[nx+1][ny].potential + cost.y
				: HIGH + cost.y;
		// WEST
		pot.z = (nx-1 >= 0)  ? potential[nx-1][ny].potential + cost.z
				: HIGH + cost.z;
		mx = (pot.y < pot.z) ? Direction.EAST : Direction.WEST;
		mx = (pot.y >= HIGH && pot.z >= HIGH) ? Direction.OUT : mx;

		if (mx == Direction.EAST && nx+1 >= w)
			mx = Direction.WEST;
		else if (mx == Direction.WEST && nx-1 < 0)
			mx = Direction.EAST;
		
		// NORTH 
		pot.w = (ny+1 < h) ? potential[nx][ny+1].potential + cost.w
				: HIGH + cost.w;
		// SOUTH
		pot.x = (ny-1 >= 0) ? potential[nx][ny-1].potential + cost.x
				: HIGH + cost.x;

		my = (pot.w < pot.x) ? Direction.NORTH : Direction.SOUTH;
		my = (pot.w >= HIGH && pot.x >= HIGH) ? Direction.OUT : my;

		if (my == Direction.NORTH && ny+1 >= h)
			my = Direction.SOUTH; 
		else if (my == Direction.SOUTH && ny-1 < 0)
			my = Direction.NORTH;
		
		//System.out.println(String.format("nx= %d, ny= %d ", nx, ny) + mx + " " + my);
		//System.out.println(pot);



		// PHI AND COST
		double cx=0;
		double cy=0;
		double phi_mx=0;
		double phi_my=0;


		// SOLVE EIKIONAL EQUATION (11)
		if (mx == Direction.OUT && my == Direction.OUT) {
			System.err.println("Error when calculating mx and my");
			return;
		}


		else if (mx != Direction.OUT && my == Direction.OUT) {
			if (mx == Direction.EAST) {
				phi_mx = potential[nx+1][ny].potential;
				cx = cost.y;
				potential[nx][ny].grad_x = -1;
			}
			else if (mx == Direction.WEST) {
				phi_mx = potential[nx-1][ny].potential;
				cx = cost.z;
				potential[nx][ny].grad_x = 1;
			}
			cy = (my == Direction.NORTH) ? cost.w : cost.x;

			potential[nx][ny].potential = phi_mx + (cx+cy);
			potential[nx][ny].grad_y = 0;
		}


		else if (mx == Direction.OUT && my != Direction.OUT) {
			cx = (mx == Direction.EAST) ? cost.y : cost.z;

			if (my == Direction.NORTH) {
				phi_my = potential[nx][ny+1].potential;
				cy = cost.w;
				potential[nx][ny].grad_y=-1;
			}
			else if (my == Direction.SOUTH) {
				phi_my = potential[nx][ny-1].potential;
				cy = cost.x;
				potential[nx][ny].grad_y=1;
			}

			potential[nx][ny].potential = phi_my + (cy+cx);
			potential[nx][ny].grad_x = 0;
		}
		else if (mx != Direction.OUT && my != Direction.OUT) {
			if (mx == Direction.EAST) {
				phi_mx = potential[nx+1][ny].potential;
				cx = cost.y;
			}
			else if (mx == Direction.WEST) {
				phi_mx = potential[nx-1][ny].potential;
				cx = cost.z;
			}
			if (my == Direction.NORTH) {
				phi_my = potential[nx][ny+1].potential;
				cy = cost.w;
			}
			else if (my == Direction.SOUTH) {
				phi_my = potential[nx][ny-1].potential;
				cy = cost.x;
			}

			double delta = 2 * (cx+cy)*(cx+cy) - (phi_mx-phi_my)*(phi_mx-phi_my);
			potential[nx][ny].potential = (delta < 0) ? (cx+cy) + Math.min(phi_mx, phi_my)
					: ((phi_mx+phi_my) + Math.sqrt(delta)) / 2.0;

			double gx = (mx == Direction.EAST) ? potential[nx+1][ny].potential-potential[nx][ny].potential
					: potential[nx][ny].potential-potential[nx-1][ny].potential;
			double gy =(my == Direction.NORTH) ? potential[nx][ny+1].potential - potential[nx][ny].potential
					: potential[nx][ny].potential - potential[nx][ny-1].potential;

			double norm = Math.sqrt((gx*gx)+(gy*gy));
			potential[nx][ny].grad_x = gx / norm;
			potential[nx][ny].grad_y = gy / norm;
		}
		
		//System.out.println(cx + " " + cy);
	}



	public void computePotentionalAndGradientMap() 
	{
		// FAST MARCHING METHOD
		int nb_known = 0;
		int nb_total_cell = w * h;

		Comparator<Potential> cmp = new Comparator<Potential>() {
			public int compare(Potential p1, Potential p2) {
				return Double.compare(p1.potential, p2.potential); 
			}
		};
		PriorityQueue<Potential> queue = new PriorityQueue<Potential>(cmp);

		// INIT
		for (int j = 0; j < h; ++j) {
			for (int i = 0; i < w; ++i) 
				potential[i][j].set(HIGH, i, j, PotentialState.UNKNOWN);
		}

		potential[goal_x][goal_y].set(0, goal_x, goal_y, PotentialState.CANDIDATE);

		queue.add(potential[goal_x][goal_y]);

		while (nb_known < nb_total_cell && !queue.isEmpty())
		{
			Potential min_pot = queue.poll();
			int x = min_pot.x;
			int y = min_pot.y;
			potential[x][y].state = PotentialState.KNOWN;
			nb_known++;


			setNeighPotential(x-1, y, queue); // WEST
			setNeighPotential(x+1, y, queue); // EAST
			setNeighPotential(x, y+1, queue); // NORTH
			setNeighPotential(x, y-1, queue); // SOUTH
		}
	}

	public void setNeighPotential(int nx, int ny, PriorityQueue<Potential> queue)
	{
		if (nx < 0 || nx >= w || ny < 0 ||ny >= h)
			return;
		if (potential[nx][ny].state == PotentialState.UNKNOWN) {
			//System.out.println(String.format("nx= %d, ny = %d, pot= %f", nx, ny, potential[nx][ny].potential));

			computePotentionalandGradientCell(nx, ny);
			queue.add(potential[nx][ny]);
		}
	}

	public void updateVelocityAndPosition()
	{
		for (int i = 0; i < agentList.size(); ++i) {
			UserAgent a = agentList.get(i);
			int cx = (int) a.x;
			int cy = (int) a.y;


			if (cx < 0 || cx >= w || cy < 0 ||cy >= h) // OUT OF MAP
				continue;

			// SPEED
			a.dir_x = -potential[cx][cy].grad_x;
			a.dir_y = -potential[cx][cy].grad_y;

			// w = NORTH; x = SOUTH; y = EAST; z = WEST
			double vx = (a.dir_x >= 0) ? speedMap[cx][cy].y
					: speedMap[cx][cy].z;
			double vy = (a.dir_y >= 0) ? speedMap[cx][cy].w
					: speedMap[cx][cy].x;

			a.speed = Math.abs(vx * a.dir_x) + Math.abs(vy * a.dir_y);

			// POSITION
			a.x += a.speed * a.dir_x * timestep;
			a.y += a.speed * a.dir_y * timestep;

			a.x = Math.max(0, a.x);
			a.x = Math.min(a.x, w-1);
			a.y = Math.max(0, a.y);
			a.y = Math.min(a.y, h-1);

			//System.out.println("spd = " + vx + " " + vy);

			space.moveTo(a, a.x, a.y);
		}
	}




}
